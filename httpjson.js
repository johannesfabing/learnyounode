var http = require('http');
var url = require('url');

server = http.createServer(function(req, res) {

    var Purl = url.parse(req.url, true)
        if (Purl.pathname == '/api/parsetime') {
            var date = new Date(Purl.query.iso);
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify({
                hour: date.getHours(),
                minute: date.getMinutes(),
                second: date.getSeconds()
            }));
        }else if(Purl.pathname == '/api/unixtime') {
            res.writeHead(200, {"Content-Type": "application/json"});
            res.end(JSON.stringify({
                unixtime: (new Date(Purl.query.iso)).getTime()
            }));
        }
        else {
            res.writeHead(404);
            res.end();
        }
  });
  server.listen(process.argv[2]);