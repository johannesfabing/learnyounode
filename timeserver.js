var net = require('net')
var date = new Date()



var server = net.createServer(function (socket) {
    var now = ''
    now += date.getFullYear() + '-'
    if(date.getMonth().length >= 10){
        now += (date.getMonth()+1)+'-'
    }else{
        now += '0'+(date.getMonth()+1)+'-'
    }
    if(date.getDate() >= 10){
        now += date.getDate()+' '
    }else{
        now += '0'+date.getDate()+' '
    }
    now+=date.getHours()+':'+date.getMinutes()+'\n';
    socket.write(now)
    socket.destroy();
})
server.listen(process.argv[2])