var http = require('http')
var fs = require('fs')

var server = http.createServer(function callback(req, res){
    req.setEncoding('utf8')
    var text = ''
    var stream = fs.createReadStream(process.argv[3])
    stream.on('data',(data)=>{
        text+=data
    })
    stream.on('end',()=>{
        res.write(text)
        server.close();
    })
})

server.listen(process.argv[2]);
