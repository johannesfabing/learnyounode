var http = require('http')

var urls =process.argv.slice(2)

var result = "";

urls.forEach(function(url){
    http.get(url, function(response){
        response.setEncoding('utf8')
        response.on('data',function(data){
            result += data
        })
        response.on('end',function(){
            result += '\n'
            if(url == urls[urls.length-1]){
                console.log(result)
            }
        })
    })
})
